#!/usr/bin/env bash

cd ~/work/pollux
#source ~/virtenvs/px/bin/activate
rm -r -f build

if [[ "$@" == "dmg" ]]
then
    python app-setup.py bdist_dmg
else
    python app-setup.py bdist_mac
fi

