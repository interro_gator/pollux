.. pollux documentation master file, created by
   sphinx-quickstart on Thu Nov  5 11:43:02 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

========================================
pollux documentation
========================================

This site provides documentation for three related projects:

1. *pollux*: a web application for doing text analysis.
2. *corpkit*, a Python backend for pollux
3. *pollux-cl*, a command-line natural language interpreter

With *pollux*, you can create parsed, structured and metadata-annotated corpora, and then search them for complex lexicogrammatical patterns. Search results can be quickly edited, sorted and visualised, saved and loaded within projects, or exported to formats that can be handled by other tools. In fact, you can easily work with any dataset in `CONLL U`_ format, including the freely available, multilingual `Universal Dependencies Treebanks`_. 

Concordancing is extended to allow the user to query and display grammatical features alongside tokens. Keywording can be restricted to certain word classes or positions within the clause. If your corpus contains multiple documents or subcorpora, you can identify keywords in each, compared to the corpus as a whole.

.. rubric:: Installation

Via pip:

.. code-block:: bash

   $ pip install pollux

via Git:

.. code-block:: bash

   $ git clone https://www.github.com/interrogator/pollux
   $ cd pollux
   $ python setup.py install

Parsing and interrogation of parse trees will also require *Stanford CoreNLP*. *pollux* can download and install it for you automatically.

.. rubric:: Running the app

After installation, pollux can be started from the command line with:

.. code-block:: bash

   # load sample project
   $ pollux-quickstart

You can parse your own corpus from within the web app, or via the command line:

.. code-block:: bash

   # parse
   $ pollux-parse path/to/corpus
   $ mkdir ~/corpora
   # add to database
   $ cp -R path/to/corpus-parsed ~/corpora
   $ pollux-build
   # open the tool
   $ pollux

*pollux-cl* is a bit like the `Corpus Workbench`_. You can open it with:

.. code-block:: bash

   $ pollux-cl
   # or, alternatively:
   $ python -m pollux.cl

And then start working with natural language commands:

.. code-block:: bash

   > set junglebook as corpus
   > parse junglebook with outname as jb
   > set jb as corpus
   > search corpus for deps matching "f/nsubj/ <- f/ROOT/"
   > calculate result as percentage of self
   > plot result as line chart with title as 'Example figure'

From the interpreter, you can enter ``ipython``, ``jupyter notebook`` or ``gui`` to switch between interfaces, preserving the local namespace and data where possible.

Information about the syntax is available at the :ref:`interpreter-page`.

.. toctree::
   :maxdepth: 2
   :caption: Web app

   docs/pollux/pollux.index.rst
   docs/pollux/pollux.explore.rst
   docs/pollux/pollux.extra.rst

.. toctree::
   :maxdepth: 1
   :caption: API

   docs/API/corpkit.building.rst
   docs/API/corpkit.interrogating.rst
   docs/API/corpkit.concordancing.rst
   docs/API/corpkit.editing.rst
   docs/API/corpkit.visualising.rst
   docs/API/corpkit.managing.rst
   
.. toctree::
   :maxdepth: 1
   :caption: Interpreter

   docs/interpreter/cl.overview.rst
   docs/interpreter/cl.setup.rst
   docs/interpreter/cl.making.rst
   docs/interpreter/cl.interrogating.rst
   docs/interpreter/cl.concordancing.rst
   docs/interpreter/cl.annotating.rst
   docs/interpreter/cl.editing.rst
   docs/interpreter/cl.visualising.rst
   docs/interpreter/cl.managing.rst

.. toctree::
   :maxdepth: 2
   :caption: API reference

   docs/API-ref/corpkit.corpus.rst

.. toctree::
   :maxdepth: 2
   :caption: External links
   :hidden:

   GitHub <https://github.com/interrogator/pollux>

.. _Stanford CoreNLP: http://stanfordnlp.github.io/CoreNLP/
.. _Corpus Workbench: http://cwb.sourceforge.net/
.. _CONLL U: http://universaldependencies.org/format.html
.. _Universal Dependencies Treebanks: https://github.com/UniversalDependencies