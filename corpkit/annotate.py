"""
corpkit: add annotations to conll-u via concordancing
"""

def process_special_annotation(v, lin):
    """
    If the user wants a fancy annotation, like 'add middle column',
    this gets processed here. it's potentially the place where the
    user could add entropy score, or something like that.
    """
    if v in lin.index:
        return str(lin[v])
    if isinstance(v, bool) or v is None:
        return str(v).lower()
    return v
    #if v.lower() not in ['i', 'index', 'm', 'scheme', 't', 'q']:
    #    return v
    #if v == 'index':
    #    return lin.name
    #elif v in ['m', 't']:
    #    return str(lin[v])
    #else:
    #    return v

def make_string_to_add(annotation, lin, replace=False, level='s'):
    """
    Make a string representing metadata to add
    """
    from corpkit.constants import STRINGTYPE
    if isinstance(annotation, STRINGTYPE):
        if replace:
            the_text = annotation + '\n'
        else:
            the_text = 'tags=' + annotation + '\n'
    
    start = str()
    for k, v in annotation.items():
        # these are special names---add more?
        v = process_special_annotation(v, lin) 
        if replace:
            start = '%s\n' % v
        else:
            start += '%s=%s\n' % (k, v)
    the_text = start
    if level == 's' and not replace:
        the_text = '# ' + the_text
    if level in ['w', 'i']:
        pass
        # not good, could possibly change a float value or soemthing...
        #the_text = '\t'.join(lin.astype(str).reset_index()[['index', 'w', 'l', 'p', 'x', ]].values) + the_text
    return the_text

def get_line_number_for_entry(data, si, ti, annotation, level='s'):
    """
    Find the place in filename at which to add the string
    """

    # this might not be good for ud corpora which define sent_id differently

    # so if s is 1, split data at first \n\n
    sent = data.strip('\n').split('\n\n', si)[si-1]
    partstart = data.index(sent)
    partend = partstart+len(sent)
    # get the line on which the entry should appear
    if level == 's':
        # get the line ix for after the sent_id
        lnum = data[:partstart].count('\n') + 2
    elif level in ['i', 'w']:
        lnum = data[:partstart].count('\n') + 1
        lnum += sent.count('\n#')
        lnum += ti
        lnum -= 1
    field = 'tags' if isinstance(annotation, str) else list(annotation.keys())[0]
    if level == 's':
        ixx = next((i for i, l in enumerate(sent.splitlines()) \
                   if l.startswith('# %s=' % field)), False)
        if ixx is False:
            return lnum, False
        else:
            return lnum + ixx - 2, True
    elif level in ['i', 'w']:
        return lnum, False
    else:
        raise NotImplementedError

def update_contents(contents, place, text, do_replace=False, level='s', annotation=False):
    """ 
    Open file, read lines, add or replace the line with the good one
    """ 
    # easy enough for sentence level
    if level == 's':
        if do_replace:
            contents[place] = contents[place].rstrip('\n').replace(text + ';', '') + ';' + text
        else:
            contents.insert(place, text)
        return contents
    elif level in ['w', 'i']:
        the_line = contents[place].rstrip('\n')
        # if it already has some annotations
        if not the_line.endswith('\t_'):
            # get the last column and the rest
            rest, last_val = the_line.rsplit('\t', 1)
            # check if the key is already in this last column
            field = list(annotation.keys())[0] if isinstance(annotation, dict) else annotation
            if '|' + field + '=' in the_line or '\t' + field + '=' in the_line:
                # if the key is there, remove it
                import re
                regex = field + '=' + '.*?(\||$)'
                last_val = re.sub(regex, '', last_val)
                the_line = rest + "\t" + last_val.lstrip('|') + text
            else:
                the_line += "|" + text
        # if it had nothing, remove the placeholder
        else:
            the_line = the_line.rstrip('_') + text

        contents[place] = the_line
        return contents

def dry_run_text(filepath, contents, place, colours, level='s'):
    """
    Show a dry run of what the annotations would be
    """
    import os
    contents[place] = contents[place].rstrip('\n') + '  <==========\n'
    try:
        contents[place] = colours['green'] + contents[place] + colours['reset']
    except:
        pass

    max_lines = next((i for i, l in enumerate(contents[place:]) if l == '\n'), 10)
    max_lines = 30 if max_lines > 30 else max_lines

    formline = '   Add metadata: %s   \n' % (os.path.basename(filepath))
    bars = '=' * len(formline)

    print(bars + '\n' + formline + bars)
    print(''.join(contents[place-3:max_lines+place]))

def annotate(open_file, contents):
    """
    Add annotation to a single file
    """
    from corpkit.constants import PYTHON_VERSION
    contents = ''.join(contents)
    if PYTHON_VERSION == 2:
        contents = contents.encode('utf-8', errors='ignore')
    open_file.seek(0)
    open_file.write(contents)
    open_file.truncate()

def delete_lines(corpus, annotation, dry_run=True, colour={}):
    """
    Show or delete the necessary lines
    """
    from corpkit.constants import OPENER, PYTHON_VERSION
    import re
    import os
    tagmode = True
    no_can_do = ['sent_id', 'parse']

    if isinstance(annotation, dict):
        tagmode = False
        for k, v in annotation.items():
            if k in no_can_do:
                print("You aren't allowed to delete '%s', sorry." % k)
                return
            if not v:
                v = r'.*?'
            regex = re.compile(r'(# %s=%s)\n' % (k, v), re.MULTILINE)
    else:
        if annotation in no_can_do:
            print("You aren't allowed to delete '%s', sorry." % k)
            return
        regex = re.compile(r'((# tags=.*?)%s;?(.*?))\n' % annotation, re.MULTILINE)

    fs = []
    for (root, dirs, fls) in os.walk(corpus):
        for f in fls:
            fs.append(os.path.join(root, f))
    
    for f in fs:
    
        if PYTHON_VERSION == 2:
            from corpkit.process import saferead
            data = saferead(f)[0]
        else:
            with open(f, 'rb') as fo:
                data = fo.read().decode('utf-8', errors='ignore')

        if dry_run:
            if tagmode:
                repl_str = r'\1 <=======\n%s\2\3 <=======\n' % colour.get('green', '')
            else:
                repl_str = r'\1 <=======\n'
            try:
                repl_str = colour['red'] + repl_str + colour['reset']
            except:
                pass
            data, n = re.subn(regex, repl_str, data)
            nspl = 100 if tagmode else 50
            delim = '<======='
            data = re.split(delim, data, maxsplit=nspl)
            toshow = delim.join(data[:nspl+1])
            toshow = toshow.rsplit('\n\n', 1)[0]
            print(toshow)
            if n > 50:
                n = n - 50
                print('\n... and %d more changes ... ' % n)

        else:
            if tagmode:
                repl_str = r'\2\3\n'
            else:
                repl_str = ''
            data = re.sub(regex, repl_str, data)
            with OPENER(f, 'w') as fo:
                from corpkit.constants import PYTHON_VERSION
                if PYTHON_VERSION == 2:
                    data = data.encode('utf-8', errors='ignore')
                fo.write(data)
                
def annotator(result, corpus_path, annotation, dry_run=True, level='s', inverse=False, deletemode=False):
    """
    Run the annotator pipeline over multiple files

    result (Results):
    corpus_path (str):
    """
    import re
    import os
    from corpkit.constants import OPENER, STRINGTYPE, PYTHON_VERSION
    colour = {}
    try:
        from colorama import Fore, init, Style
        init(autoreset=True)
        colour = dict(green=Fore.GREEN, reset=Style.RESET_ALL, red=Fore.RED)
    except ImportError:
        pass
    # probably broken
    if deletemode:
        delete_lines(result, corpus_path, annotation, dry_run=dry_run, colour=colour)
        return

    # this is all set up for sent level tagging
    # list of f s i tuples
    if 'index' not in result.columns:
        result = result.reset_index()

    file_sent_words = result[['file', 's', 'i', 'index']].values.tolist()
    from collections import defaultdict
    outt = defaultdict(list)
    for ixx, (f, s, i, n) in enumerate(file_sent_words):
        outt[f].append((int(s), int(i), int(n), ixx))
    
    # for each unique file
    for i, (fname, entries) in enumerate(sorted(outt.items()), start=1):
        if not os.path.isfile(fname):
            fname = os.path.join(corpus_path, fname)
        with OPENER(fname, 'r+') as fo:
            # get file contents
            data = fo.read()
            contents = [i + '\n' for i in data.split('\n')]
            # for each token in the result index
            for si, ti, ni, index in list(reversed(sorted(set(entries)))):
                # get the line number
                line_num, do_replace = get_line_number_for_entry(data, si, ti, annotation, level=level)
                anno_text = make_string_to_add(annotation, result.iloc[index], replace=do_replace, level=level)
                contents = update_contents(contents, line_num, anno_text, do_replace=do_replace, level=level, annotation=annotation)
                if dry_run and i < 50:
                    dry_run_text(fname,
                                 contents,
                                 line_num,
                                 level=level,
                                 colours=colour)
            if not dry_run:
                annotate(fo, contents=contents)
        if not dry_run:
            print('%d annotations made in %s' % (len(entries), fname))
        if dry_run and i > 50:
            break

    if dry_run:
        if len(file_sent_words) > 50:
            n = len(file_sent_words) - 50
            print('... and %d more changes ... ' % n)

        