Installing pollux
==================

pollux can be downloaded, installed and run locally.

Via Git
--------

```shell
git clone https://github.com/interrogator/pollux
cd pollux
python setup.py install
pollux serve && pollux open
```

Via pip
---------

```shell
pip install pollux
pollux serve && pollux open
```

With Docker
------------

For Docker, you'll have to specify the data paths when you start the container.

```shell
#todo
```