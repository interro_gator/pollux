import setuptools
import sys
from cx_Freeze import setup, Executable

from setuptools.command.install import install
import os
from os.path import isfile, isdir, join, dirname
import pollux
version = pollux.__version__

base = None
if sys.platform == "win32":
    base = "Win32GUI"

import distutils
import opcode
import os

plist = """
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>CFBundleDevelopmentRegion</key>
    <string>English</string>
    <key>CFBundleExecutable</key>
    <string>start</string>
    <key>CFBundleIconFile</key>
    <string>icon.icns</string>
    <key>LSUIElement</key>
    <true/>
</dict>
</plist>
"""

# opcode is not a virtualenv module, so we can use it to find the stdlib; this is the same
# trick used by distutils itself it installs itself into the virtualenv
distutils_path = os.path.join(os.path.dirname(opcode.__file__), 'distutils')
import numpy
numpy_path = os.path.dirname(numpy.__file__)

from PyObjCTools import AppHelper
apphelp = os.path.dirname(AppHelper.__file__)

build_options = {"packages": ["asyncio", "jinja2.ext", "jinja2", "tables", "objc", "rumps"],
                 "excludes": ["matplotlib", "distutils", "ipython", 'numpy'],
                 'include_files': [(distutils_path, 'distutils'), 'pollux/static/Info.plist', 'pollux/corpora/',
                 'pollux/static/', 'pollux/templates/', (numpy_path, 'lib/python3.6/numpy'), (apphelp, 'lib/python3.6/PyObjCTools')]} #'pollux/corpora',

build_options_mac = {'iconfile': os.path.join(os.getcwd(), "pollux/static/favicon.icns"),
                     'custom_info_plist': 'pollux/static/Info.plist'}
build_options_dmg = {'volume_label': 'pollux', 'applications_shortcut': True}

# extras
hdf = ["tables"]
backend = ['tabview>=1.4.0', 'matplotlib>=1.4.3', 'blessings>=1.6']

#mainfile = "pollux/scripts/run.py"
mainfile = "pollux/start.py"

#initpath = 
setup(name='pollux',
      version=version,
      description='Sophisticated corpus linguistics',
      url='http://github.com/interrogator/pollux',
      author='Daniel McDonald',
      include_package_data=True,
      zip_safe=False,
      options = {"build_exe": build_options, "bdist_mac": build_options_mac, "bdist_dmg": build_options_dmg},
      executables = [Executable(mainfile, base=base)],
      packages=['corpkit', 'corpkit.download', 'pollux'],
      scripts=['corpkit/new_project', 'pollux/scripts/pollux-parse', 'pollux/scripts/pollux',
               'pollux/scripts/pollux-build', 'pollux/scripts/pollux-quickstart',
               'corpkit/corpkit', 'corpkit/corpkit.1'],
      package_dir={'corpkit': 'corpkit', 'pollux': 'pollux'},
      package_data={'corpkit': ['*.jar', 'corpkit/*.jar', '*.sh', 'corpkit/*.sh', 
                                '*.ipynb', 'corpkit/*.ipynb', '*.p', 'dictionaries/*.p',
                                '*.py', 'dictionaries/*.py', '*.properties', 'langprops/*.properties']},
      author_email='mcddjx@gmail.com',
      license='MIT',
      #cmdclass={'install': CustomInstallCommand},
      keywords=['corpus', 'linguistics', 'nlp'],
      install_requires=[#"matplotlib>=1.4.3",
                        "nltk>=3.0.0",
                        "joblib",
                        "rumps",
                        "pandas>=0.19.2",
                        "requests",
                        "tabview>=1.4.0",
                        "chardet",
                        "blessings>=1.6",
                        "tqdm>=4.11.2",
                        "Flask>=0.12.1",
                        "flask-Bootstrap",
                        "flask-WTF",
                        "numpy>=1.12.0",
                        "traitlets>=4.1.0"],
    #extras_require={
    #    'HDF5':  hdf,
    #    'backend': backend,
    #    'all': backend + hdf
    #},
      dependency_links=['git+https://github.com/interrogator/tabview.git#egg=tabview-1.4.0',
                        'git+https://github.com/interrogator/tqdm.git#egg=tqdm-4.11.2'])
