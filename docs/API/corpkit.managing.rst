Managing projects
=================

``corpkit`` has a few other bits and pieces designed to make life easier when doing corpus linguistic work. This includes methods for loading saved data, for working with multiple corpora at the same time, and for switching between command line and graphical interfaces. Those things are covered here.

.. contents::
   :local:

Loading saved data
-------------------

When you're starting a new session, you probably don't want to start totally from scratch. It's handy to be able to load your previous work. You can load data in a few ways.

First, you can use ``corpkit.load()``, using the name of the filename you'd like to load. By default, ``corpkit`` looks in the ``saved_interrogations`` directory, but you can pass in an absolute path instead if you like.

.. code-block:: python

   >>> import corpkit
   >>> nouns = corpkit.load('nouns')

Second, you can use ``corpkit.loader()``, which provides a list of items to load, and asks the user for input:

.. code-block:: python

   >>> nouns = corpkit.loader()

Third, when instantiating a ``Corpus`` object, you can add ``load_saved=True`` keyword argument to load any saved data belonging to this corpus as an attribute.

.. code-block:: python

   >>> corpus = Corpus('data/psyc-parsed', load_saved=True)

A final alternative approach stores all interrogations within an :class:`corpkit.interrogation.Interrodict` object object:

.. code-block:: python

   >>> r = corpkit.load_all_results()
