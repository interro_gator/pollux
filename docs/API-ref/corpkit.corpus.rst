Corpus classes
=====================

`Corpus`
---------------------

A corpus is an unparsed or parsed collection of files that can be searched, or brought into memory for higher performance operations.

.. autoclass:: corpkit.corpus.Corpus
    :members:
    :undoc-members:
    :show-inheritance:

`File`
---------------------

Corpora are comprised of files, which can be turned into pandas DataFrames and manipulated.

.. autoclass:: corpkit.corpus.File
    :members:
    :undoc-members:
    :show-inheritance:

`LoadedCorpus`
---------------------

The `load` method of Corpus objects returns a MultiIndexed DataFrame, with three levels: filename, sentence number, and token number. This object can be searched very quickly, because all data is in memory.

.. autoclass:: corpkit.corpus.LoadedCorpus
    :members:
    :undoc-members:
    :show-inheritance:


`Results`
---------------------

Searching a corpus returns an object that can be searched again, turned into tables or concordance lines, or exported to other formats.

.. autoclass:: corpkit.interrogation.Results
    :members:
    :undoc-members:
    :show-inheritance:
